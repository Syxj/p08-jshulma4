package com.example.jorda.threelanes;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;


public class StartMenuActivity extends AppCompatActivity implements View.OnClickListener {

    //Make button to start game
    private ImageButton buttonPlay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_menu_activity);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //NEED TO ADD BUTTON IMAGE
       // buttonPlay = (ImageButton) findViewById(R.id.buttonPlay);

       // buttonPlay.setOnClickListener(this);

        //Putting this here for now
        startActivity(new Intent(this, GameActivity.class));
    }

    @Override
    public void onClick(View v)
    {
        //Start game
        startActivity(new Intent(this, GameActivity.class));
    }

}
