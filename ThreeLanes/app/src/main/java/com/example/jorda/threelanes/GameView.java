package com.example.jorda.threelanes;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by jorda on 5/5/2017.
 */

public class GameView extends SurfaceView implements Runnable {

    volatile boolean playing;

    //game thread
    private Thread gameThread = null;

    private Player player;

    //Objects used for drawing
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;
    private int backgroundColor;

    //for making enemies
    private Context cont;
    private int screX;
    private int screY;

    private ArrayList<Enemy> enemies;
    private ArrayList<Projectile> projectiles;

    private int numEnemies = 0;
    private int numLetThrough = 0;
    private int numProjectiles = 0;

    //Three collision detectors, for hurting the player
    private Rect playerOne;
    private Rect playerTwo;
    private Rect playerThree;

    //Bitmaps for healthbar
    private Bitmap healthGreen;
    private Bitmap healthYellow;
    private Bitmap healthRed;
    private Bitmap healthBorder;
    private Bitmap healthTmp;

    //Info for healthbar
    private double percentHealth;

    //Background
    private Bitmap background;

    private MediaPlayer mp;

    //Class constructor
    public GameView(Context context, int screenX, int screenY)
    {
        super(context);

        screX = screenX;
        screY = screenY;
        cont = context;

        player = new Player(context, screenX, screenY);
        surfaceHolder = getHolder();
        paint = new Paint();
        backgroundColor = Color.rgb(205,133,63);

        enemies = new ArrayList<Enemy>();
        projectiles = new ArrayList<Projectile>();

        playerOne = new Rect(player.getBitmap().getWidth(),player.getBitmap().getHeight(),player.getBitmap().getWidth()*2,player.getBitmap().getHeight()*2);
        playerTwo = new Rect(player.getBitmap().getWidth(),player.getMaxY()/2,player.getBitmap().getWidth()*2,(player.getMaxY()/2) + (player.getBitmap().getHeight()));
        playerThree = new Rect(player.getBitmap().getWidth(),player.getMaxY()+10,player.getBitmap().getWidth()*2,(player.getMaxY()+10)+(player.getBitmap().getHeight()));

        //Bitmap healthbars
        healthGreen = BitmapFactory.decodeResource(context.getResources(), R.drawable.health_green);
        healthYellow = BitmapFactory.decodeResource(context.getResources(), R.drawable.health_yellow);
        healthRed = BitmapFactory.decodeResource(context.getResources(), R.drawable.health_red);
        healthBorder = BitmapFactory.decodeResource(context.getResources(), R.drawable.health_border);
        healthTmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.health_green);

        percentHealth = 1;

        background = BitmapFactory.decodeResource(context.getResources(), R.drawable.background);
        background = Bitmap.createScaledBitmap(background, screX, screY, false);
    }

    @Override
    public void run()
    {
        while(true) {
            if (playing)
            {
                //determine if any projectiles hit
                dealDamage();

                //create enemies
                createEnemies();

                //update the frame
                update();

                //draw the frame
                draw();

                //control
                control();
            }
        }
    }

    private void update()
    {
        //Update the enemies locations
        if(!enemies.isEmpty())
        {
            //ArrayList<Enemy> tmpArray = new ArrayList<Enemy>();
            for(Enemy ene : enemies)
            {
                if(ene.getDraw()) {
                    ene.update();
                    if (Rect.intersects(ene.getDetectCollision(), playerOne) || Rect.intersects(ene.getDetectCollision(), playerTwo) || Rect.intersects(ene.getDetectCollision(), playerThree)) {
                        numLetThrough++;
                        // tmpArray.add(ene);
                        player.takeDamage(ene.getDamage());
                        if (player.getHealth() <= 0) {
                            playSound(R.raw.game_over);
                            gameOver();
                            return;
                        } else {
                            playSound(R.raw.player_hit);
                            ene.setDraw();
                        }
                    }
                }
            }
            //if(!tmpArray.isEmpty()) enemies.removeAll(tmpArray);
        }

        //Update the projectile locations
        if(!projectiles.isEmpty()) {
          //  ArrayList<Projectile> tmpArray2 = new ArrayList<Projectile>();
            for (Projectile proj : projectiles) {
                if (proj.update()) {
                   // tmpArray2.add(proj);
                    numProjectiles--;
                }
            }
      //      if (!tmpArray2.isEmpty()) projectiles.removeAll(tmpArray2);
        }
    }

    private void draw() {
        if (playing) {
            if (surfaceHolder.getSurface().isValid()) {

                canvas = surfaceHolder.lockCanvas();

                canvas.drawBitmap(
                        background,
                        0,
                        0,
                        paint);

                //draw the player
                canvas.drawBitmap(
                        player.getBitmap(),
                        player.getX(),
                        player.getY(),
                        paint);

                //draw the enemies
                if (!enemies.isEmpty()) {
                    for (Enemy ene : enemies) {
                        if (ene.getDraw()) {
                            canvas.drawBitmap(
                                    ene.getBitmap(),
                                    ene.getX(),
                                    ene.getY(),
                                    paint);
                        }
                    }
                }
                //draw the projectiles
                if (!projectiles.isEmpty()) {
                    for (Projectile proj : projectiles) {
                        if(proj.getDraw()) {
                            canvas.drawBitmap(
                                    proj.getBitmap(),
                                    proj.getX(),
                                    proj.getY(),
                                    paint);
                        }
                    }
                }

                //draw the healthbar
                canvas.drawBitmap(
                        healthBorder,
                        (screX * 4) / 11,
                        10,
                        paint);

                if (player.getHealthPercent() != percentHealth) {
                    percentHealth = player.getHealthPercent();
                    if (percentHealth > .60) {
                        healthTmp = Bitmap.createScaledBitmap(healthGreen, (int) (healthGreen.getWidth() * percentHealth), healthGreen.getHeight(), false);
                    } else if (percentHealth > .30) {
                        healthTmp = Bitmap.createScaledBitmap(healthYellow, (int) (healthYellow.getWidth() * percentHealth), healthYellow.getHeight(), false);
                    } else if (percentHealth >= 0) {
                        healthTmp = Bitmap.createScaledBitmap(healthRed, (int) (healthRed.getWidth() * percentHealth), healthRed.getHeight(), false);
                    }
                }
                canvas.drawBitmap(
                        healthTmp,
                        ((screX * 4) / 11) + 1,
                        11,
                        paint);
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    //We will update with roughly 60fps
    private void control()
    {
        try
        {
            gameThread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //don't want the game to update while we are paused
    public void pause()
    {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {/*nothing*/}
    }

    public void resume()
    {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent)
    {
        if(playing) {
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_UP:
                    //let go
                    if (x < screX / 2) {
                        if (y < screY / 3) {
                            player.setLane(1);
                        } else if (y < (screY * 2) / 3) {
                            player.setLane(2);
                        } else {
                            player.setLane(3);
                        }
                    } else {
                        createProjectile();
                    }
                    break;
                case MotionEvent.ACTION_DOWN:
                    //pressed
                    break;
            }
        }
        else
        {
            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_UP:
                    //let go
                    player = new Player(cont, screX, screY);
                    enemies = new ArrayList<Enemy>();
                    numEnemies = 0;
                    numLetThrough = 0;
                    projectiles = new ArrayList<Projectile>();
                    numProjectiles = 0;
                    percentHealth = 1;
                    healthTmp = BitmapFactory.decodeResource(cont.getResources(), R.drawable.health_green);
                    playing = true;
                    System.out.println("We were here");
                    break;

                case MotionEvent.ACTION_DOWN:
                    //pressed
                    break;
            }
        }
        return true;
    }

    //Taking paint settings from last Android project with Pete and Jasper
    public void gameOver()
    {
        playing = false;
        Paint textPaint = new Paint();
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(150);
        textPaint.setFakeBoldText(true);
        textPaint.setColor(Color.WHITE);
        if(surfaceHolder.getSurface().isValid())
        {

            canvas = surfaceHolder.lockCanvas();
            //will change to a background once we get the basis working
            canvas.drawColor(Color.BLACK);
            canvas.drawText("Game Over", screX/2, screY/2, textPaint);
            textPaint.setTextSize(90);
            canvas.drawText("You killed " + (numEnemies - numLetThrough) + " enemies", screX/2, screY/2+100, textPaint);
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    public void dealDamage()
    {
        //ArrayList<Projectile> tmpArray = new ArrayList<Projectile>();
       // ArrayList<Enemy> tmpArray2 = new ArrayList<Enemy>();
        if(!projectiles.isEmpty()) {
            for (Projectile proj : projectiles) {
                if(proj.getDraw()) {
                    if (!enemies.isEmpty()) {
                        for (Enemy ene : enemies) {
                            if(ene.getDraw()) {
                                if (Rect.intersects(proj.getDetectCollision(), ene.getDetectCollision())) {
                                    if (ene.takeDamage(proj.getDamage())) {
                                        //tmpArray2.add(ene);
                                        ene.setDraw();
                                        playSound(R.raw.enemy_hit);
                                    }
                                    //tmpArray.add(proj);
                                    proj.setDraw();
                                    numProjectiles--;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            //if(!tmpArray.isEmpty()) projectiles.removeAll(tmpArray);
          //  if(!tmpArray2.isEmpty()) enemies.removeAll(tmpArray2);
        }
    }

    public void createEnemies()
    {
        Random generator = new Random();
        if(generator.nextInt(1000) >= 989)
        {
            numEnemies++;
            double multiplier = 1;
            if(numEnemies > 15) multiplier = 1.25;
            else if (numEnemies > 25) multiplier = 1.5;
            else if (numEnemies > 35) multiplier = 1.75;
            else if (numEnemies > 50) multiplier = 2;
            Enemy tmpEnemy = new Enemy(cont, screX, screY, multiplier);
            enemies.add(tmpEnemy);
        }
    }

    public void createProjectile()
    {
        if(numProjectiles <= 10) {
            Projectile tmpProj = new Projectile(cont, screX, screY, player.getDamage(), player.getLane());
            projectiles.add(tmpProj);
            numProjectiles++;
        }
    }

    //Taking from p07 with Peter and Jasper
    public void playSound(int sound)
    {
        if(mp != null)
        {
            mp.stop();
            mp.release();
        }
        mp = MediaPlayer.create(cont, sound);
        if(mp != null)
        {
            mp.start();
        }
    }

}
