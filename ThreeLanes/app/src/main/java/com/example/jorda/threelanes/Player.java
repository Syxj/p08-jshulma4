package com.example.jorda.threelanes;

/**
 * Created by jorda on 5/5/2017.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Player {

    //player's image
    private Bitmap bitmap;

    //player's coordinates
    private int x;
    private int y;

    //screen height (width)
    private int maxY;

    private int health;
    private double maxHealth;
    private int damage;

    private int lane;

    //constructor
    public Player(Context context, int screenX, int screenY) {
        //set image
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.player);

        //get boundaries
        maxY = screenY - bitmap.getHeight();
        x = bitmap.getWidth();
        y = maxY+10;

        health = 100;
        maxHealth = 100.0;
        damage = 3;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getMaxY() {return maxY; }

    public int getDamage() { return damage; }

    public void takeDamage(int dmg) { health -= dmg; }

    public int getHealth() { return health; }

    public double getHealthPercent() { return health/maxHealth; }

    public int getLane() { return lane; }

    public void setLane(int newLane)
    {
        lane = newLane;
        if(lane == 1) {y = bitmap.getHeight();}
        else if(lane == 2){y = maxY/2;}
        else{y=maxY+10;}
    }
}
