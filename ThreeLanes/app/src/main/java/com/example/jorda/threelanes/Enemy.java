package com.example.jorda.threelanes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.Random;

/**
 * Created by jorda on 5/5/2017.
 */

public class Enemy {
    private Bitmap bitmap;
    private int x;
    private int y;

    private int dx;

    private int maxX;
    private int maxY;

    private int lane;

    private Rect detectCollision;

    private int health;
    private int damage;

    private boolean draw = true;

    public Enemy(Context context, int screenX, int screenY, double multiplier)
    {
        //generate one of three types of enemies: sword, bow, magic. Type difference only health, damage, and look
        Random generator = new Random();
        int type = generator.nextInt(3);
        if(type == 0) //sword
        {
            health = 20;
            damage = 10;
            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.enemy_sword);
        }
        else if (type == 1) //bow
        {
            health = 15;
            damage = 20;
            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.enemy_bow);
        }
        else //magic
        {
            health = 10;
            damage = 30;
            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.enemy_magic);
        }

        maxX = screenX - bitmap.getWidth();
        maxY = screenY - bitmap.getHeight();

        x = maxX;

        lane = generator.nextInt(3) + 1;
        if(lane == 1){y = bitmap.getHeight();}
        else if(lane == 2){y = maxY/2;}
        else{y = maxY+10;}

        dx = (int)(15 * multiplier);

        detectCollision = new Rect(x,y,bitmap.getWidth(),bitmap.getHeight());
    }

    public void update()
    {
        x -= dx;

        //hit detection
        detectCollision.left = x;
        detectCollision.top = y;
        detectCollision.right = x + bitmap.getWidth();
        detectCollision.bottom = y + bitmap.getHeight();
    }

    public boolean takeDamage(int damage)
    {
        health -= damage;
        if(health <= 0)
        {
            return true;
        }
        return false;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public Rect getDetectCollision() {
        return detectCollision;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDamage() { return damage; }

    public boolean getDraw() { return draw; }
    public void setDraw() { draw = false; }

}
