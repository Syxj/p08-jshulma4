package com.example.jorda.threelanes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.Random;

/**
 * Created by jorda on 5/7/2017.
 */

public class Projectile {
    private Bitmap bitmap;
    private int x;
    private int y;

    private int dx = 25;

    private int maxX;
    private int maxY;
    private int minX;

    private Rect detectCollision;

    private int damage;

    private boolean draw = true;

    public Projectile(Context context, int screenX, int screenY, int dmg, int lane)
    {
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.projectile);

        maxX = screenX - bitmap.getWidth();
        maxY = screenY - bitmap.getHeight();
        minX = bitmap.getWidth()+100;

        x = minX;

        if(lane == 1){y = bitmap.getHeight()*3;}
        else if(lane == 2){y = maxY/2;}
        else{y = maxY+10;}

        damage = dmg;

        detectCollision = new Rect(x,y,bitmap.getWidth(),bitmap.getHeight());
    }

    public boolean update()
    {
        if(draw) {
            x += dx;

            //hit detection
            detectCollision.left = x;
            detectCollision.top = y;
            detectCollision.right = x + bitmap.getWidth();
            detectCollision.bottom = y + bitmap.getHeight();
            if (x > maxX) {
                draw = false;
                return true;
            }
        }
        return false;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public Rect getDetectCollision() {
        return detectCollision;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDamage() { return damage; }

    public boolean getDraw() { return draw; }
    public void setDraw() { draw = false; }
}
